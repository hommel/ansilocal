FROM alpine:3.9

# Install bash, ansible, and dependencies
RUN apk add --no-cache \
    ansible \
    bash \
    openssh-client

COPY ./docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["ansible"]
