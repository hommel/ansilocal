#!/bin/bash -e
#
# This script will check the docker host's IP address on the (default)
# bridge network and will add it as a file that *could* be used as the
# inventory file. Usage, however, will be left to the commands or the
# configuration file used in conjunction with running commands.
#
# In other words, it's here if you need it.

host=$(ip route | grep via | awk '{print $3}')
echo "docker-host ansible_host=$host" > /tmp/docker_host

exec "$@"
